import Policies from './policies';
export default{
    install(Vue, options) {
        authorize.Vue.prototype.authorize=function (policy,model){
            if(!window.Auth.singIn) return false;
        
            if(typeof policy=='string' && typeof model=='object'){
                const user=window.Auth.user;
                return Policies[policy](user,model);
            }
        };
        Vue.prototype.singIn=window.Auth.singIn;
    }
}

