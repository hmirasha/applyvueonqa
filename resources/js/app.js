/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//import Authorization from './authorization/authorize';
import Policies from './authorization/policies';
require('./bootstrap');

window.Vue = require('vue');
//Vue.use('Authorization');


Vue.prototype.authorize=function (policy,model){
    if(!window.Auth.singIn) return false;

    if(typeof policy=='string' && typeof model=='object'){
        const user=window.Auth.user;
        return Policies[policy](user,model);
    }
};
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('user-info', require('./components/userInfo.vue').default);
Vue.component('answer', require('./components/answer.vue').default);
Vue.component('vote', require('./components/vote.vue').default);
Vue.component('answers', require('./components/answers.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
