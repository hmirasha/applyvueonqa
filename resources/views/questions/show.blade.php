@extends('layouts.app')

@section('content') 
<div class="container">
    <div class="row ">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <div class="card-title">
                	<div class="d-flex align-item-center">
                		<h2>{{$question->title}}</h2>
                		<div class="ml-auto">
                			<a href="{{route('questions.index')}}" class="ml-3 btn btn-outline-secondary">Back to Questions</a>
                		</div>
                	</div>
                </div>
                <hr>
                <div class="media">
                  <!-- @include('shared._vote',[
                       'model'=>$question,
                   ])-->
                    <vote name="question" :model="{{$question}}"></vote> 
                    <div class="media-body leftBorder">
                          {!!$question->body_html!!}
                        <div class="row">
                            <div class="col-md-4">
                                @include('shared._change',[
                                'model'=>$question,
                                'canEdit'=>'update-question',
                                'canDelete'=>'delete-question',
                                ])
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4 float-right">
                             <user-info :model="{{$question}}" label="Asked"></user-info>
                            </div>
                        </div>
                    </div>                 
                </div>
            </div>
        </div>
    </div>
</div>
<answers :question="{{$question}}"></answers>
</div>
@endsection
