@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                	<div class="d-flex align-item-center">
                		<h2>All Questions</h2>
                        @if(Auth::check())
                		<div class="ml-auto">
                			<a href="{{route('questions.create')}}" class="btn btn-outline-secondary">Create Question</a>
                		</div>
                        @endif
                	</div>
                </div>
                <div class="card-body">
                    @include('layouts._messages')
                    @if(empty($questions))
                    <strong>
                        No questions available ...
                    </strong>
                    @else
                    	@foreach($questions as $question)
                    		<div  class="media split">
                    			<div class="counters d-flex flex-column">
                    				<div class="vote">
                    					<strong>{{$question->votes_count}}</strong>{{Str::plural('votes', $question->votes_count)}}
                    				</div>
                    				<div class="status {{$question->status}}">
                    					<strong>{{$question->answers_count}}</strong>{{Str::plural('answer', $question->answers_count)}}
                    				</div>
                    				<div class="view">
                    					{{$question->views." ".Str::plural('view', $question->views)}}
                    				</div>
                    			</div>
                    			<div class="media-body leftBorder">
                                    <div class="d-flex align-item-center">
                                        <h3 class="mt-0"><a href="{{$question->url}}">{{$question->title}}</h3></a>
                                        <div class="ml-auto">
                                            @can('update-question',$question)
                                            <a href="{{route('questions.edit',$question->id)}}" class="btn btn-outline-info btn-sm">Edit</a>
                                            @endcan
                                            @can('delete-question',$question)
                                            <form action="{{route('questions.destroy',$question->id)}}" method="post" class="formDelete">
                                                @method('Delete')
                                                @csrf
                                                <button type="submit" class="btn btn-sm btn-outline-danger" onclick="return confirm('Are You Sure?')">Delete</button>
                                            </form>
                                            @endcan
                                        </div>
                                    </div>
    								<p class="lead">
    									Asked By 
    									<a href="{{$question->user->url}}">{{$question->user->name}}</a>
    									<small class="text-muted">{{$question->created_date}}</small>
    								</p>
    								{{$question->excerpt}}
    							</div>
    						</div>
    					@endforeach
                        <div class="mx-auto">
                        {{$questions->links()}}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
