    <div class="row justify-content-center">
        <div class="col-md-12">
                    <div class="card-title">
                    	<div class="d-flex align-item-center">
                    		<h4>Create Answer</h4>
                    	</div>
                        
                    </div>
                   <form method="POST" action="{{route('questions.answers.store',[$question->id])}}">
                    @csrf
                       <div class="form-group">
                            <textarea type="text" name="body" class="form-control {{$errors->has('body')?'is-invalid':''}}"  rows="10">{{old('body')}}</textarea>
                               @if($errors->has('body'))
                                   <div class="invalid-feedback">
                                        <strong>{{$errors->first('body')}}</strong>
                                    </div>
                                @endif
                        </div>
                        <div class="form-group">
                            <button class="btn btn-outline-primary byn-lg">Save answer</button>
                        </div>
                   </form>
        </div>
    </div>