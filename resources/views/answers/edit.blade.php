@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                	<div class="d-flex align-item-center">
                		<h2>Edite Answer for question : {{$question->title}}</h2>
                		<div class="ml-auto">
                			<a href="{{route('questions.show',$question->slug)}}" class="btn btn-outline-secondary">Back to Question</a>
                		</div>
                	</div>
                </div>
                <div class="card-body">
                   <form method="POST" action="{{route('questions.answers.update',[$question->id,$answer->id])}}">
                    @csrf
                    {{method_field('Put')}}
                    <div class="form-group">
                        <textarea type="text" name="body" id="answer-body" class="form-control {{$errors->has('body')?'is-invalid':''}}" rows="10">{{old('body',$answer->body)}}
                        </textarea>
                       @if($errors->has('body'))
                           <div class="invalid-feedback">
                                <strong>{{$errors->first('body')}}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                            <button class="btn btn-outline-primary byn-lg">Save answer</button>
                   </div>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
