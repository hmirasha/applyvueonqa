<answer :answer="{{$answer}}" inline-template v-cloak>
    <div class="media split">
        <vote name="answer" :model="{{$answer}}"></vote> 
        <div class="media-body pt-2 leftBorder">
            <form v-if="editing" class="form-group" @submit.prevent="upadte">
                <textarea name="body" v-model="body" class="form-control mb-2" rows="10"></textarea>
                <div class="float-right">
                  <button  class="btn btn-outline-primary btn-sm" :disabled="isValid">Save</button>
                  <button @click="cancel" type="button" class="btn btn-outline-danger btn-sm">Cancel</button>
                </div>
            </form>
            <div v-else>
                <div v-html="bodyHtml"></div>
                <div class="row">
                    <div class="col-md-4">
                        @can('update-question',$answer)
                            <a @click="edit" class="btn btn-outline-info btn-sm">Edit</a>
                        @endcan
                
                        @can('delete-question',$answer)
                        <button type="submit" class="btn btn-sm btn-outline-danger" @click="destroy">Delete</button>
                        @endcan
                    </div>
                    
                    <div class="col-md-4"></div>
                    <div class="col-md-4 float-right">
                        @include('shared._author',[
                            'model'=>$answer,
                            'label'=>'Answered',
                        ])
                    </div>
                </div>            
            </div>
        </div>
    </div>
</answer>
