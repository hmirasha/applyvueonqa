   <div class="row mt-4 ">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    @if($answerCount==0)
                        @if(!Auth::check())
                           Login in then add on <a href="{{route('login')}}">Click Here</a> 
                        @else
                           <strong>No answers available ... add one</strong>
                        @endif
                    @else
                     <div class="card-title ">
                        <h4>{{$answerCount}} {{Str::plural('answer',$question->answers_count)}}</h4>
                    </div>
                    <hr>
                     @include('layouts._messages')
                        @foreach($answers as $answer)
                            @include('answers._answer',['answer'=>$answer])
                        @endforeach
                    @endif
                </div>
            </div>
            @if(Auth::check())
                <div class="card">
                    <div class="card-body">
                       @include('answers._createAnswer')
                   </div>
                </div>
            @endif
            </div>
    </div>