<a title="Favorite This {{$name}} or not" 
     class="{{\Auth::guest()?'off':'move '.($model->is_favorited?'favorite':'unfavorite')}}"
     onclick="event.preventDefault();document.getElementById('favorite-{{$name}}-{{$model->id}}').submit();" >
  <i class="fas fa-star fa-2x"></i>
  <span class="favorite-count">
    {{$model->favorutes_count}}
  </span>
</a>
<form id="favorite-question-{{$model->id}}" style="display: none;" method="POST" action="/{{$urlSegement}}/{{$model->id}}/favorite">
  @csrf
  @if($model->is_favorited)
    @method('DELETE')
  @endif
</form>