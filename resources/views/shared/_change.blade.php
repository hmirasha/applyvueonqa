@can($canEdit,$model)
        <a href="{{route('questions.edit',$model)}}" class="btn btn-sm btn-outline-primary">Edit</a>
@endcan
@can($canDelete,$model)
    <form action="{{route('questions.destroy',$model->id)}}" method="post" class="formDelete">
	    @method('Delete')
	    @csrf
	    <button type="submit" class="btn btn-sm btn-outline-danger" onclick="return confirm('Are You Sure?')">
	        Delete
	    </button>
    </form>
@endcan