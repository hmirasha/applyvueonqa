@can('accept',$model)
  <a title="This {{$name}} is accepted"
    class="{{\Auth::guest()?'':'move'.' '.$model->status}} "
    onclick="event.preventDefault();document.getElementById('accepte-answer-{{$model->id}}').submit();"
  >
    <i class="fas fa-check fa-2x"></i>
  </a>
  <form method="POST" id="accepte-answer-{{$model->id}}" action="{{route('answers.accept',$model->id)}}" style="display:none;">
    @csrf
  </form>
    @else
@if($model->isBest)
    <a title="This answer is accepted"
     class="{{$model->status}}" 
    >
      <i class="fas fa-check fa-2x"></i>
    </a>
@endif
@endcan