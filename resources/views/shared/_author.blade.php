<span class="text-muted">
    {{$label .' '. $model->created_date}}
</span>
<div class="media mt-2">
    <a href="{{$model->user->url}}">
        <img src="{{$model->user->avatar}}">
    </a>
    <div class="media-body ml-2">
        <a class="mt-1" href="{{$model->user->url}}">
            {{$model->user->name}}
        </a>
    </div>
</div>