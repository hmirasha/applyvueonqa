@if($model instanceof App\Question)
  @php
     $name='question';
     $urlSegement='questions';
  @endphp
@elseif($model instanceof App\Answer)
  @php
    $name='answer';
    $urlSegement='answers';
  @endphp
@endif

 <div class="d-flex flex-column voteControl">
  <a title="This {{$name}} is useful"
     class="{{\Auth::guest()?'off':'move '. $model->isvote(1)}}"
     onclick="event.preventDefault();document.getElementById('vote-up-{{$name}}-{{$model->id}}').submit();" >
    <i class="fas fa-caret-up fa-3x"></i>
  </a>
  <form id="vote-up-{{$name}}-{{$model->id}}" style="display: none;" method="POST" action="/{{$urlSegement}}/{{$model->id}}/vote">
    @csrf
    <input type="hidden" name="vote" value="1">
  </form>
  <span class="{{\Auth::guest()?'off':'move '.'vote-count'}}">
    {{$model->votes_count}}
  </span>
  <a title="This {{$name}} is unuseful" 
      class="{{\Auth::guest()?'off':'move '.$model->isvote(-1)}}"
      onclick="event.preventDefault();document.getElementById('vote-down-{{$name}}-{{$model->id}}').submit();" >
    <i class="fas fa-caret-down fa-3x"></i>
  </a>
  <form id="vote-down-{{$name}}-{{$model->id}}" style="display: none;" method="POST" action="/{{$urlSegement}}/{{$model->id}}/vote">
    @csrf
    <input type="hidden" name="vote" value="-1">
  </form>
@if($model instanceof App\Question)
    <favorite :question="{{$model}}"></favorite>
@elseif($model instanceof App\Answer)
    <accept :answer="{{$model}}"></accept>
@endif
</div>