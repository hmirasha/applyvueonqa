<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use Illuminate\Support\Facades\Auth;

class FavoriteQuestionController extends Controller
{
	function __construct($foo = null)
	{
		$this->middleware('auth');
	}
    public function store(Question $question)
    {
        $question->favorites()->attach(Auth::user()->id);
        if(request()->expectsJson()){
            return response()->json([null,204]);
        }
        return back();
    }

    public function destroy(Question $question)
    {
    	$question->favorites()->detach(Auth::user()->id);
        if(request()->expectsJson()){
            return response()->json([null,204]);
        }
        return back();
    }
}
