<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use Illuminate\Support\Facades\Auth;

class VoteQuestionController extends Controller
{
    function __construct()
    {
    	$this->middleware('auth');
    }
    public function __invoke(Question $question)
    {
    	$vote=  (int) request()->vote;

        Auth::user()->voteQuestion($question,$vote);
        if(request()->expectsJson()){
            return response()->json([
                'message' =>'Thanks foe feedback',
                'votesCount'=>$question->votes_count]);
            } 

    	return back();
    }
}
