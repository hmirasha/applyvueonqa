<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Answer;


class VoteAnswerController extends Controller
{
    public function __constructor()
    {
    	$this->middleware('auth');
    }

    public function __invoke(Answer $answer)
    {
    	$vote=  (int) request()->vote;

    	Auth::user()->voteAnswer($answer,$vote);
        if(request()->expectsJson()){
            return response()->json([
                'message' =>'Thanks foe feedback',
                'votesCount'=>$answer->votes_count]);
            } 
    	return back();
    }
}
