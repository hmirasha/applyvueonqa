<?php

namespace App;
trait Votabletrait{

    public function votes()
    {
        return $this->morphToMany(User::class,'votable')->withTimestamps();
    }

    public function voteUp()
    {
        return $this->votes()->wherePivot('vote',1);
    }

     public function voteDown()
    {
        return $this->votes()->wherePivot('vote',-1);
    }

    public function isvote($value)
    {
        return $this->votes()->where('user_id',\Auth::id())->wherePivot('vote',$value)->exists()?"voted":"unVoted";
    }
}