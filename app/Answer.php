<?php

namespace App;
use App\User;
use App\Question;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use Votabletrait, BodyTrait;
    protected $fillable=['body','user_id'];

    protected $appends=['created_date','body_html','is_best'];

    public function question()
    {
    	return $this->belongsTo(Question::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getBodyHtmlAttribute()
    {
        return  parsedown($this->body);
    }
    protected static function boot()
    {
        parent::boot();

        static::created(function($answer)
        {
            $answer->question->increment('answers_count');
        });

        static::deleted(function($answer)
        {
            $question=$answer->question;
            $question->decrement('answers_count');
            if($question->best_answer_id==$answer->id){
                $question->best_answer_id=null;
                $question->save();
            }
        });
    }

    public function getStatusAttribute()
    {
        return $this->isBest() ?"accept":"";
    }

    public function getIsBestAttribute()
    {
        return $this->isBest();
    }

    public function isBest()
    {
       return $this->id==$this->question->best_answer_id;
    }

     public function getIsvoteAttribute()
    {
       return $this->isvote();
    }
  
}
