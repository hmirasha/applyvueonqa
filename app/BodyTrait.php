<?php

namespace App;
trait BodyTrait{
    public function getExcerptAttribute()
    {
        return $this->excerpt(350);
    }

    public function excerpt($value)
    {
        return str_limit(strip_tags($this->bodyHtml()),$value);
    }

    private function bodyHtml()
    {
        return parsedown($this->body);
    }
}