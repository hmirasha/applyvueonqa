<?php

namespace App;
use App\User;
use App\Answer;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use Votabletrait, BodyTrait;
    protected $fillable=['title','body'];

    protected $appends=['created_date','url','is_favorited','favorutes_count'];
	
	public function user()
	{
		return $this->belongsto(User::class);
	}
	public function setTitleAttribute($value)
	{
		$this->attributes['title']=$value;
		$this->attributes['slug']=Str::slug($value);
	}
	public function getUrlAttribute()
	{
		return route('questions.show',$this->slug);
	}
	public function getCreatedDateAttribute()
	{
		return $this->created_at->diffForHumans();
	}
	public function getStatusAttribute()
	{
		if($this->answers_count>0){
           if ($this->best_answer_id) {
           	 return 'accepted';
           }
           return 'answered';
		}
		return 'unanwered';
	}
	public function getBodyHtmlAttribute()
	{
		return  $this->bodyHtml();
	}
	
    public function answers()
    {
        return $this->hasMany(Answer::class)->orderBy('votes_count','DESC');
    }
    
    public function acceptBestAnswer(Answer $answer)
    {
    	$this->best_answer_id=$answer->id;
    	$this->save();
    }

    public function favorites()
    {
    	return $this->belongsToMany(User::class,'favorites')->withTimestamps();
    }

    public function isFavorited()
    {
   	    return $this->favorites()->where('user_id',\Auth::id())->count()>0;
    }

    public function getIsFavoritedAttribute()
    {
    	return $this->isFavorited();
    }

    public function getFavorutesCountAttribute()
    {
    	return $this->favorites->count();
    }

  /*  public function getExcerptAttribute()
    {
        return $this->excerpt(350);
    }

    public function excerpt($value)
    {
        return str_limit(strip_tags($this->bodyHtml()),$value);
    }

    private function bodyHtml()
    {
        return parsedown($this->body);
    }*/
    
    public function getIsvoteAttribute()
    {
       return $this->isvote();
    }
 //    public function getVotableDownAttribute()
   // {
     //   return $this->voteDown()?"vote-down":"";;
   // }

}
