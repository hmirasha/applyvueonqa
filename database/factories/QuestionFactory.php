<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        //
        'title'=>$title=rtrim($faker->sentence(rand(5,10)),'.'),
        'slug'=>Str::slug($title),
        'body'=>$faker->paragraphs(rand(3,7),true),
        'views'=>rand(0,20),
        //'votes_count'=>rand(-5,5),
        'answers_count'=>rand(0,10),
    ];
});
