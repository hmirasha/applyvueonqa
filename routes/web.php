<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//$2y$10$slYJhSHKIuo0dMbnSQ8HwOxXa9gpGda69ryHtorvl36/knIu9flwK
/*
Route::get('/',function ()
{
	return Hash::make(111);
});

Route::get('test',function ()
{
	return \Auth::id();
	//view('test');
});

//$2y$10$XUgBekTpGMnEEPhSDeSvZewyfPlfn0Oeq7NQk6joVkUv1iY1XiDm.
*/
Route::get('/','QuestionsController@index');

Auth::routes();

Route::get('/home', 'QuestionsController@index')->name('home');

Auth::routes();

Route::get('/home', 'QuestionsController@index')->name('home');

Auth::routes();

Route::resource('questions', 'QuestionsController')->except('show');
Route::get('questions/{slug}', 'QuestionsController@show')->name('questions.show');
Route::resource('questions.answers', 'AnswersController')->except(['create','show']);
Route::post('/answers/{answer}/accept','AcceptAnswerController')->name('answers.accept');

Route::post('/questions/{question}/favorite','FavoriteQuestionController@store')->name('answers.favorite');
Route::delete('/questions/{question}/favorite','FavoriteQuestionController@destroy')->name('answers.unfavorite');

Route::post('/questions/{question}/vote','VoteQuestionController');
Route::post('/answers/{answer}/vote','VoteAnswerController');